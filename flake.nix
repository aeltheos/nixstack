{
  description = "Everything required (someday) to run openstack on nixos.";

  inputs = {
    # Waiting for https://github.com/NixOS/nixpkgs/pull/284113.
    nixpkgs.url = "github:ybeaugnon/nixpkgs/stestrCheckHook";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, flake-utils, nixpkgs }: flake-utils.lib.eachDefaultSystem (system:
  let
    # We add the missing openstack packages via an overlays.
    overlay = final: prev: {
      pythonPackagesExtensions = prev.pythonPackagesExtensions ++ [(import ./pkgs)];
    };

    pkgs = import nixpkgs { inherit system; overlays = [ overlay ]; };
  in {

    # Ugly hack for dev.
    packages = pkgs;

    devShells.default = pkgs.mkShell {
      buildInputs = with pkgs; [
	python3
	python3Packages.requests
	python3Packages.requirements-parser
	python3Packages.jinja2
      ];
    };
  });
}
