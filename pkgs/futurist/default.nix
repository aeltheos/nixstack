{ lib
, buildPythonPackage  
, fetchPypi  
, eventlet  
, oslotest  
, stestrCheckHook  
, testtools  
, pre-commit  
, doc8  
, prettytable  
, testscenarios  
, python-subunit  
, coverage  
}: buildPythonPackage rec {
  pname = "futurist";
  version = "2.4.1";

  src = fetchPypi {
    pname = "futurist";
    inherit version;
    sha256 = "sha256-nBdgqHfA/jJg0EtqbUNSptJaxY5IPx1s1JXjPcN0D/c=";
  };

  nativeCheckInputs = [
    eventlet
    oslotest
    stestrCheckHook
    testtools
    pre-commit
    doc8
    prettytable
    testscenarios
    python-subunit
    coverage
  ];

  importChecks = [ "futurist" ];

  meta = with lib; {
    description = "";
    homepage = "https://docs.openstack.org/futurist/latest/";
    license = licenses.asl20;
  };
}
