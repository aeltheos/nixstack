{ lib
, buildPythonPackage  
, fetchPypi  
, pbr  
, oslo-utils  
, oslo-log  
, oslo-config  
, prometheus-client  
, oslotest  
, stestrCheckHook  
, hacking  
, bandit  
, coverage  
}: buildPythonPackage rec {
  pname = "oslo-metrics";
  version = "0.7.0";

  src = fetchPypi {
    pname = "oslo.metrics";
    inherit version;
    sha256 = "sha256-jGUmmjC6PpVSd5lXQkIeFPOIeUWw7FIVGANOUnP1lJo=";
  };

  propagatedBuildInputs = [
    pbr
    oslo-utils
    oslo-log
    oslo-config
    prometheus-client
  ];

  nativeCheckInputs = [
    oslotest
    stestrCheckHook
    hacking
    bandit
    coverage
  ];

  importChecks = [ "oslo-metrics" ];

  meta = with lib; {
    description = "";
    homepage = "https://opendev.org/openstack/oslo.metrics";
    license = licenses.asl20;
  };
}
