{ lib
, buildPythonPackage  
, fetchPypi  
, oslo-config  
, oslo-i18n  
, prettytable  
, oslo-utils  
, oslo-policy  
, oslotest  
, stestrCheckHook  
, pre-commit  
, hacking  
, oslo-serialization  
}: buildPythonPackage rec {
  pname = "oslo-upgradecheck";
  version = "2.2.0";

  src = fetchPypi {
    pname = "oslo.upgradecheck";
    inherit version;
    sha256 = "sha256-tI/J/kthmz5Cneas9kczU9dLCRNeOu5qOrtSDb/CXCg=";
  };

  propagatedBuildInputs = [
    oslo-config
    oslo-i18n
    prettytable
    oslo-utils
    oslo-policy
  ];

  nativeCheckInputs = [
    oslotest
    stestrCheckHook
    pre-commit
    hacking
    oslo-serialization
  ];

  importChecks = [ "oslo-upgradecheck" ];

  meta = with lib; {
    description = "";
    homepage = "http://launchpad.net/oslo";
    license = licenses.asl20;
  };
}
