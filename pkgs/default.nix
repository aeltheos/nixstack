self: super: with self; {
  etcd3gw = callPackage ./etcd3gw {};

  futurist = callPackage ./futurist {};

  keystone = callPackage ./keystone {};

  keystonemiddleware = callPackage ./keystonemiddleware {};

  oslo-cache = callPackage ./oslo-cache {};

  oslo-messaging = callPackage ./oslo-messaging {};

  oslo-metrics = callPackage ./oslo-metrics {};

  oslo-middleware = callPackage ./oslo-middleware {};

  oslo-policy = callPackage ./oslo-policy {};

  oslo-service = callPackage ./oslo-service {};

  oslo-upgradecheck = callPackage ./oslo-upgradecheck {};

  osprofiler = callPackage ./osprofiler {};

  pycadf = callPackage ./pycadf {};

  python-binary-memcached = callPackage ./python-binary-memcached {
    inherit (self.pkgs) memcached;
  };

  python-qpid-proton = callPackage ./python-qpid-proton {};

  pyngus = callPackage ./pyngus {};

  uhashring = callPackage ./uhashring {};
}
