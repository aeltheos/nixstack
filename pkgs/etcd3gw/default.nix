{ lib
, buildPythonPackage  
, fetchPypi  
, pbr  
, requests  
, futurist  
, pifpaf  
, urllib3  
, pytestCheckHook  
, oslotest  
, testtools  
, extras  
, hacking  
, testscenarios  
, python-subunit  
, coverage  
, nose  
, testrepository  
}: buildPythonPackage rec {
  pname = "etcd3gw";
  version = "2.3.0";

  src = fetchPypi {
    pname = "etcd3gw";
    inherit version;
    sha256 = "sha256-H4Z63RpMz2uNA+a2LpRF4DsDq0vQOk6BDgdW0pnlRsI=";
  };

  propagatedBuildInputs = [
    pbr
    requests
    futurist
  ];

  nativeCheckInputs = [
    pifpaf
    urllib3
    pytestCheckHook
    oslotest
    testtools
    extras
    hacking
    testscenarios
    python-subunit
    coverage
    nose
    testrepository
  ];

  importChecks = [ "etcd3gw" ];

  meta = with lib; {
    description = "";
    homepage = "https://docs.openstack.org/etcd3gw/latest/";
    license = licenses.asl20;
  };
}