{ lib
, buildPythonPackage  
, fetchFromGitHub
, uhashring
, pytestCheckHook
, six
, trustme
, mistune
, memcached
}: buildPythonPackage rec {
  pname = "python-binary-memcached";
  version = "0.31.2";

  # Missing conftest.py on sdist.
  src = fetchFromGitHub {
    owner = "jaysonsantos";
    repo = "python-binary-memcached";
    rev = "v" + version;
    sha256 = "sha256-+o6jP6gI1gRhLLz7JhNTspqmpW+7MHiRJUeyD1PHwbo=";
  };

  propagatedBuildInputs = [
    uhashring
    memcached
  ];

  nativeCheckInputs = [
    pytestCheckHook
    six
    memcached
    trustme
    mistune
  ];

  importChecks = [ "python-binary-memcached" ];
 
  meta = with lib; {
    description = "";
    homepage = "https://github.com/jaysonsantos/python-binary-memcached";
    license = licenses.asl20;
  };
}
