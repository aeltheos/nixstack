{ lib
, buildPythonPackage  
, fetchPypi  
, oslo-config  
, oslo-serialization  
, pytz  
, six  
, debtcollector  
, sphinxcontrib-apidoc  
, flake8-docstrings  
, stestrCheckHook  
, testtools  
, hacking  
, python-subunit  
, coverage  
, sphinx  
, openstackdocstheme  
, fixtures  
}: buildPythonPackage rec {
  pname = "pycadf";
  version = "3.1.1";

  src = fetchPypi {
    pname = "pycadf";
    inherit version;
    sha256 = "sha256-K+SRFD0h+j3R+ELOjdSXPpX1gIk4h5YPqlwRphkAgeg=";
  };

  propagatedBuildInputs = [
    oslo-config
    oslo-serialization
    pytz
    six
    debtcollector
  ];

  nativeCheckInputs = [
    sphinxcontrib-apidoc
    flake8-docstrings
    stestrCheckHook
    testtools
    hacking
    python-subunit
    coverage
    sphinx
    openstackdocstheme
    fixtures
  ];

  importChecks = [ "pycadf" ];

  meta = with lib; {
    description = "";
    homepage = "https://docs.openstack.org/pycadf/latest/";
    license = licenses.asl20;
  };
}
