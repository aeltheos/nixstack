{ lib
, buildPythonPackage  
, fetchPypi
, cffi
}: buildPythonPackage rec {
  pname = "python-qpid-proton";
  version = "0.39.0";

  src = fetchPypi {
    pname = "python-qpid-proton";
    inherit version;
    sha256 = "sha256-NiBVrmq0x/FDckfGAnV/MDKNVcCmmG1baMqXmN6fzgI=";
  };

  propagatedBuildInputs = [
    cffi
  ];

  nativeCheckInputs = [
  ];

  importChecks = [ "python-qpid-proton" ];

  doCheck = false; # FIXME

  meta = with lib; {
    description = "";
    homepage = "http://qpid.apache.org/proton/";
    license = licenses.asl20;
  };
}
