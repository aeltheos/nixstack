{ lib
, buildPythonPackage  
, fetchPypi  
, netaddr  
, oslo-concurrency  
, oslo-serialization  
, oslo-utils  
, prettytable  
, requests  
, webob  
, importlib-metadata  
, elasticsearch  
, ddt  
, opentelemetry-exporter-otlp  
, redis  
, pymongo  
, jaeger-client  
, stestrCheckHook  
, testtools  
, docutils  
, hacking  
, pre-commit  
, bandit  
, flake8-import-order  
, coverage  
, opentelemetry-sdk  
}: buildPythonPackage rec {
  pname = "osprofiler";
  version = "4.1.0";

  src = fetchPypi {
    pname = "osprofiler";
    inherit version;
    sha256 = "sha256-V4N7YgWBsTREouIFZHddT8c+/2fHiIr5mUftWC/lFLA=";
  };

  propagatedBuildInputs = [
    netaddr
    oslo-concurrency
    oslo-serialization
    oslo-utils
    prettytable
    requests
    webob
    importlib-metadata
  ];

  nativeCheckInputs = [
    elasticsearch
    ddt
    opentelemetry-exporter-otlp
    redis
    pymongo
    stestrCheckHook
    testtools
    docutils
    hacking
    pre-commit
    bandit
    flake8-import-order
    coverage
    opentelemetry-sdk
  ];

  doCheck = false; # FIXME Tests depends on deprecated library.

  importChecks = [ "osprofiler" ];

  meta = with lib; {
    description = "";
    homepage = "https://docs.openstack.org/osprofiler/latest/";
    license = licenses.asl20;
  };
}
