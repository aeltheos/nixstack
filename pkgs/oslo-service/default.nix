{ lib
, buildPythonPackage  
, fetchPypi  
, webob  
, debtcollector  
, eventlet  
, fixtures  
, greenlet  
, oslo-utils  
, oslo-concurrency  
, oslo-config  
, oslo-log  
, oslo-i18n  
, pastedeploy  
, routes  
, paste  
, yappi  
, oslotest  
, pre-commit  
, stestrCheckHook  
, doc8  
, hacking  
, bandit  
, coverage  
, requests
, procps
}: buildPythonPackage rec {
  pname = "oslo-service";
  version = "3.3.0";

  src = fetchPypi {
    pname = "oslo.service";
    inherit version;
    sha256 = "sha256-RMppOV8k2I2VmZovS6N7eOeQSqoEsd0//PhvnE55Xkk=";
  };

  propagatedBuildInputs = [
    webob
    debtcollector
    eventlet
    fixtures
    greenlet
    oslo-utils
    oslo-concurrency
    oslo-config
    oslo-log
    oslo-i18n
    pastedeploy
    routes
    paste
    yappi
  ];

  nativeCheckInputs = [
    oslotest
    pre-commit
    stestrCheckHook
    doc8
    hacking
    bandit
    coverage
    requests
    procps
  ];

  disabledTests = [ # FIXME or atleast understand why this fail.
    "oslo_service.tests.test_wsgi.TestWSGIServerWithSSL.test_ssl_server"
    "oslo_service.tests.test_wsgi.TestWSGIServerWithSSL.test_app_using_ipv6_and_ssl"
    "oslo_service.tests.test_wsgi.TestWSGIServerWithSSL.test_two_servers"
    "oslo_service.tests.test_wsgi.TestWSGIServerWithSSL.test_socket_options_for_ssl_server"
  ];

  importChecks = [ "oslo-service" ];

  meta = with lib; {
    description = "";
    homepage = "https://docs.openstack.org/oslo.service/latest/";
    license = licenses.asl20;
  };
}
