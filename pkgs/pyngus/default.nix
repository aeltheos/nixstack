{ lib
, buildPythonPackage 
, fetchPypi
, python-qpid-proton
}: buildPythonPackage rec {
  pname = "pyngus";
  version = "2.3.1";

  src = fetchPypi {
    pname = "pyngus";
    inherit version;
    sha256 = "sha256-2SWGhjf8rytqIXQTfTmvkJ0elfXIonyBa1W1ogTpJA0=";
  };

  propagatedBuildInputs = [
    python-qpid-proton
  ];

  nativeCheckInputs = [
  ];

  importChecks = [ "pyngus" ];

  meta = with lib; {
    description = "";
    homepage = "https://github.com/kgiusti/pyngus";
    license = licenses.asl20;
  };
}
