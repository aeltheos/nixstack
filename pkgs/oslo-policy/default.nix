{ lib
, buildPythonPackage  
, fetchPypi  
, requests  
, oslo-config  
, oslo-context  
, oslo-i18n  
, oslo-serialization  
, pyyaml  
, stevedore  
, oslo-utils  
, requests-mock  
, oslotest  
, stestrCheckHook  
, coverage  
, sphinx  
}: buildPythonPackage rec {
  pname = "oslo-policy";
  version = "4.2.1";

  src = fetchPypi {
    pname = "oslo.policy";
    inherit version;
    sha256 = "sha256-a+Z9EL8UZOvDW53GOJaiQB4zXjUDnVd1wOxHKEXRMog=";
  };

  propagatedBuildInputs = [
    requests
    oslo-config
    oslo-context
    oslo-i18n
    oslo-serialization
    pyyaml
    stevedore
    oslo-utils
  ];

  nativeCheckInputs = [
    requests-mock
    oslotest
    stestrCheckHook
    coverage
    sphinx
  ];

  importChecks = [ "oslo-policy" ];

  meta = with lib; {
    description = "";
    homepage = "https://docs.openstack.org/oslo.policy/latest/";
    license = licenses.asl20;
  };
}
