{ lib
, buildPythonPackage  
, fetchPypi  
, pbr  
, futurist  
, oslo-config  
, oslo-log  
, oslo-utils  
, oslo-serialization  
, oslo-service  
, stevedore  
, debtcollector  
, cachetools  
, webob  
, pyyaml  
, amqp  
, kombu  
, oslo-middleware  
, oslo-metrics  
, pifpaf  
, eventlet  
, confluent-kafka  
, oslotest  
, pre-commit  
, stestrCheckHook  
, testtools  
, hacking  
, pyngus  
, bandit  
, testscenarios  
, greenlet  
, coverage  
, fixtures  
}: buildPythonPackage rec {
  pname = "oslo-messaging";
  version = "14.6.0";

  src = fetchPypi {
    pname = "oslo.messaging";
    inherit version;
    sha256 = "sha256-cjUSdI1iQOgUNG/vzCJdHStPS0rjEbsFbUR0Nk6zu6g=";
  };

  propagatedBuildInputs = [
    pbr
    futurist
    oslo-config
    oslo-log
    oslo-utils
    oslo-serialization
    oslo-service
    stevedore
    debtcollector
    cachetools
    webob
    pyyaml
    amqp
    kombu
    oslo-middleware
    oslo-metrics
  ];

  nativeCheckInputs = [
    pifpaf
    eventlet
    confluent-kafka
    oslotest
    pre-commit
    stestrCheckHook
    testtools
    hacking
    pyngus
    bandit
    testscenarios
    greenlet
    coverage
    fixtures
  ];

  importChecks = [ "oslo-messaging" ];

  meta = with lib; {
    description = "";
    homepage = "https://docs.openstack.org/oslo.messaging/latest/";
    license = licenses.asl20;
  };
}
