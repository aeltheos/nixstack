{ lib
, buildPythonPackage  
, fetchFromGitHub
, hatch
}: buildPythonPackage rec {
  pname = "uhashring";
  version = "2.3";
  
  format = "pyproject";

  src = fetchFromGitHub {
    owner = "ultrabug";
    repo = "uhashring";
    rev = version;
    sha256 = "sha256-q3yOEORurxHm/TRwNwWNPUPl97b/20z0Md962ezBjKU=";
  };

  buildInputs = [
    hatch
  ];
  propagatedBuildInputs = [
  ];

  nativeCheckInputs = [
  ];

  importChecks = [ "uhashring" ];

  meta = with lib; {
    description = "";
    homepage = "None";
    license = licenses.bsd3;
  };
}
