{ lib
, buildPythonPackage  
, fetchPypi  
, keystoneauth1  
, oslo-cache  
, oslo-config  
, oslo-context  
, oslo-i18n  
, oslo-log  
, oslo-serialization  
, oslo-utils  
, pbr  
, pycadf  
, pyjwt  
, python-keystoneclient  
, requests  
, webob  
, oslo-messaging  
, webtest  
, stevedore  
, requests-mock  
, flake8-docstrings  
, testresources  
, oslotest  
, stestrCheckHook  
, testtools  
, hacking  
, bandit  
, cryptography  
, coverage  
, python-memcached  
, fixtures  
}: buildPythonPackage rec {
  pname = "keystonemiddleware";
  version = "10.5.0";

  src = fetchPypi {
    pname = "keystonemiddleware";
    inherit version;
    sha256 = "sha256-5btHn4Gysqiz02fkprdjg/+vWKgBhKDGEtyEcwjeGnQ=";
  };

  propagatedBuildInputs = [
    keystoneauth1
    oslo-cache
    oslo-config
    oslo-context
    oslo-i18n
    oslo-log
    oslo-serialization
    oslo-utils
    pbr
    pycadf
    pyjwt
    python-keystoneclient
    requests
    webob
  ];

  nativeCheckInputs = [
    oslo-messaging
    webtest
    stevedore
    requests-mock
    flake8-docstrings
    testresources
    oslotest
    stestrCheckHook
    testtools
    hacking
    bandit
    cryptography
    coverage
    python-memcached
    fixtures
  ];

  importChecks = [ "keystonemiddleware" ];

  meta = with lib; {
    description = "";
    homepage = "https://docs.openstack.org/keystonemiddleware/latest/";
    license = licenses.asl20;
  };
}
