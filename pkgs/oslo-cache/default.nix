{ lib
, buildPythonPackage  
, fetchPypi  
, dogpile-cache  
, oslo-config  
, oslo-i18n  
, oslo-log  
, oslo-utils  
, pifpaf  
, pymongo  
, etcd3gw  
, pymemcache  
, oslotest  
, stestrCheckHook  
, pre-commit  
, hacking  
, bandit  
, python-binary-memcached  
, python-memcached  
}: buildPythonPackage rec {
  pname = "oslo-cache";
  version = "3.6.0";

  src = fetchPypi {
    pname = "oslo.cache";
    inherit version;
    sha256 = "sha256-cDJpp7N6dg1dw089bipUB4kTfUbn2lvh7Ul2jSPH2IM=";
  };

  propagatedBuildInputs = [
    dogpile-cache
    oslo-config
    oslo-i18n
    oslo-log
    oslo-utils
  ];

  nativeCheckInputs = [
    pifpaf
    pymongo
    etcd3gw
    pymemcache
    oslotest
    stestrCheckHook
    pre-commit
    hacking
    bandit
    python-binary-memcached
    python-memcached
  ];

  importChecks = [ "oslo-cache" ];

  meta = with lib; {
    description = "";
    homepage = "https://docs.openstack.org/oslo.cache/latest";
    license = licenses.asl20;
  };
}
