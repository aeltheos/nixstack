{ lib
, buildPythonPackage  
, fetchPypi  
, pbr  
, webob  
, flask  
, flask-restful  
, cryptography  
, sqlalchemy  
, stevedore  
, passlib  
, python-keystoneclient  
, keystonemiddleware  
, bcrypt  
, scrypt  
, oslo-cache  
, oslo-config  
, oslo-context  
, oslo-messaging  
, oslo-db  
, oslo-i18n  
, oslo-log  
, oslo-middleware  
, oslo-policy  
, oslo-serialization  
, oslo-upgradecheck  
, oslo-utils  
, oauthlib  
, pysaml2  
, pyjwt  
, dogpile-cache  
, jsonschema  
, pycadf  
, msgpack  
, osprofiler  
, pytz  
, webtest  
, flake8-docstrings  
, lxml  
, freezegun  
, tempest  
, oslotest  
, stestrCheckHook  
, testtools
, testresources
, testscenarios
, bashate  
, hacking  
, coverage  
, fixtures  
, requests
, ldap
, ldappool
, python-memcached
, pymongo
}: buildPythonPackage rec {
  pname = "keystone";
  version = "24.0.0";

  src = fetchPypi {
    pname = "keystone";
    inherit version;
    sha256 = "sha256-RDLY9FymO/CvF/nGTVsKnpv90qozQ6lAwU7xs6M+Eco=";
  };

  propagatedBuildInputs = [
    pbr
    webob
    flask
    flask-restful
    cryptography
    sqlalchemy
    stevedore
    passlib
    python-keystoneclient
    keystonemiddleware
    bcrypt
    scrypt
    oslo-cache
    oslo-config
    oslo-context
    oslo-messaging
    oslo-db
    oslo-i18n
    oslo-log
    oslo-middleware
    oslo-policy
    oslo-serialization
    oslo-upgradecheck
    oslo-utils
    oauthlib
    pysaml2
    pyjwt
    dogpile-cache
    jsonschema
    pycadf
    msgpack
    osprofiler
    pytz
    ldappool
    ldap
  ];

  passthru.optional-depencies = lib.fix (self: {
    memcache = [
      python-memcached
    ];

    mongodb = [
      pymongo
    ];
  });

  nativeCheckInputs = [
    webtest
    flake8-docstrings
    lxml
    freezegun
    tempest
    oslotest
    stestrCheckHook
    testtools
    testresources
    bashate
    hacking
    coverage
    fixtures
    requests
    ldap
    python-memcached
    pymongo
    testscenarios
  ];

  disabledTests = [ #FIXME
    "keystone.tests.unit.test_v3_federation.WebSSOTests.test_issue_unscoped_token_with_remote_from_protocol"
    "keystone.tests.unit.test_v3_federation.WebSSOTests.test_federated_sso_auth"
    "keystone.tests.unit.test_v3_federation.WebSSOTests.test_render_callback_template"
    "keystone.tests.unit.test_v3_federation.WebSSOTests.test_identity_provider_specific_federated_authentication"
    "keystone.tests.unit.test_v3_federation.SAMLGenerationTests.test_saml_signing"
    "keystone.tests.unit.test_policy.GeneratePolicyFileTestCase.test_policy_generator_from_command_line"
  ];

  importChecks = [ "keystone" ];

  meta = with lib; {
    description = "";
    homepage = "https://docs.openstack.org/keystone/latest";
    license = licenses.asl20;
  };
}
