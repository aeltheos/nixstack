{ lib
, buildPythonPackage  
, fetchPypi  
, pbr  
, jinja2  
, oslo-config  
, oslo-context  
, oslo-i18n  
, oslo-utils  
, stevedore  
, webob  
, debtcollector  
, statsd  
, bcrypt  
, oslotest  
, stestrCheckHook  
, pre-commit  
, testtools  
, hacking  
, oslo-serialization  
, bandit  
, coverage  
, fixtures  
}: buildPythonPackage rec {
  pname = "oslo-middleware";
  version = "6.0.0";

  src = fetchPypi {
    pname = "oslo.middleware";
    inherit version;
    sha256 = "sha256-eXAnAw4hDQKwR1wUENGBJnd6ksceGlUbNfTCSOufZR0=";
  };

  propagatedBuildInputs = [
    pbr
    jinja2
    oslo-config
    oslo-context
    oslo-i18n
    oslo-utils
    stevedore
    webob
    debtcollector
    statsd
    bcrypt
  ];

  nativeCheckInputs = [
    oslotest
    stestrCheckHook
    pre-commit
    testtools
    hacking
    oslo-serialization
    bandit
    coverage
    fixtures
  ];

  importChecks = [ "oslo-middleware" ];

  meta = with lib; {
    description = "";
    homepage = "https://docs.openstack.org/oslo.middleware/latest/";
    license = licenses.asl20;
  };
}
